package hptcg;

import hptcg.constants.ManaType;

import java.util.HashMap;
import java.util.Map;

public class Mana {

    private Map<ManaType, Integer> numberByType = new HashMap<>();

    public Mana() {
    }

    public Mana(ManaType manaType, int number) {
        this.numberByType.put(manaType, number);
    }

}
