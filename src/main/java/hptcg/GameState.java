package hptcg;

public enum GameState {

    WAITING_FOR_OPPONENT,
    PLAYING;

}
