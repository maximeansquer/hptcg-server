package hptcg.constants;

public enum CardType {

    LESSON,
    CREATURE,
    SPELL;

}
