package hptcg.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hptcg.DatabaseException;
import hptcg.User;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.net.URISyntaxException;

@Service
public class Database {

    private final String LOCAL_URI = "redis://h:p8ad185fbe5bd975ceb886a3c02a02268f7dfc71498334acb21852a9a9244785b@ec2-34-247-98-164.eu-west-1.compute.amazonaws.com:13019";

    private Jedis jedis;
    private ObjectMapper objectMapper = new ObjectMapper();

    @PostConstruct
    private void setJedisConnection() throws URISyntaxException {
        URI redisURI = new URI(getURI());
        jedis = new Jedis(redisURI);
    }

    private String getURI() {
        final String envValue = System.getenv("REDIS_URL");
        return envValue != null ? envValue : LOCAL_URI;
    }

    public void addUser(User user) throws JsonProcessingException, DatabaseException {
        if (jedis.get(user.getUsername()) != null) {
            throw new DatabaseException("The user " + user.getUsername() + " already exists.");
        }
        jedis.append(user.getUsername(), objectMapper.writeValueAsString(user));
    }

}
