package hptcg;

import com.fasterxml.jackson.core.JsonProcessingException;
import hptcg.config.Database;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;
import java.security.Principal;

@RestController
public class UserController {

    private Database database;

    @Autowired
    public void setDatabase(Database database) {
        this.database = database;
    }

    @PostMapping("/users")
    public void createGame(final Principal principal, final @RequestBody User user) throws DatabaseException, JsonProcessingException, URISyntaxException {
        database.addUser(user);
    }

}
