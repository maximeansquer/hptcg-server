package hptcg;

import hptcg.constants.CardType;

import java.util.EnumSet;
import java.util.UUID;

public abstract class HptcgObjectImpl implements HptcgObject {

    protected UUID objectId;

    protected String name;
    protected EnumSet<CardType> cardType = EnumSet.noneOf(CardType.class);

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getIdName() {
        return getName() + " [" + getId().toString().substring(0, 3) + ']';
    }

    @Override
    public String getLogName() {
        return getName();
    }

    @Override
    public String getImageName() {
        return getName();
    }

    @Override
    public void setName(String name) {
        this.name = name;

    }

    @Override
    public EnumSet<CardType> getCardType() {
        return cardType;
    }

    @Override
    public UUID getId() {
        return objectId;
    }
}
