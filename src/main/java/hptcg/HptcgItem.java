package hptcg;

import java.io.Serializable;
import java.util.UUID;

/**
 * @author BetaSteward_at_googlemail.com
 */
@FunctionalInterface
public interface HptcgItem extends Serializable {

    UUID getId();
}
