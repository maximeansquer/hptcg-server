package hptcg.abilities.mana;

import hptcg.abilities.ActivatedAbilityImpl;

/**
 *
 * @author BetaSteward_at_googlemail.com
 */
public abstract class ActivatedManaAbilityImpl extends ActivatedAbilityImpl implements ManaAbility {

}
