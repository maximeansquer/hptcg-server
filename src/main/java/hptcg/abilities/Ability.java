package hptcg.abilities;

import hptcg.game.Controllable;
import org.springframework.boot.SpringApplication;

import java.io.Serializable;

public interface Ability extends Controllable, Serializable {
}
