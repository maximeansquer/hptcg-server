package hptcg.cards.basiclessons;

import hptcg.abilities.mana.ActivatedManaAbilityImpl;
import hptcg.cards.CardImpl;
import hptcg.cards.CardSetInfo;
import hptcg.constants.CardType;

import java.util.UUID;

public abstract class BasicLesson extends CardImpl {

    public BasicLesson(UUID ownerId, CardSetInfo setInfo, ActivatedManaAbilityImpl mana) {
        super(ownerId, setInfo, new CardType[]{CardType.LESSON}, null);
    }

}
