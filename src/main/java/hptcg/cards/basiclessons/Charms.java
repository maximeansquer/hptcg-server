package hptcg.cards.basiclessons;

import hptcg.Mana;
import hptcg.abilities.mana.CharmsManaAbility;
import hptcg.cards.CardSetInfo;

import java.util.UUID;

import static hptcg.constants.ManaType.CHARMS;

public class Charms extends BasicLesson {

    public Charms(UUID ownerId, CardSetInfo setInfo) {
        super(ownerId, setInfo, new CharmsManaAbility());
    }

    @Override
    public Mana getManaContribution() {
        return new Mana(CHARMS, 1);
    }
}
