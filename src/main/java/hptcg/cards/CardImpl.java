package hptcg.cards;

import hptcg.HptcgObjectImpl;
import hptcg.Mana;
import hptcg.constants.CardType;
import hptcg.constants.SpellAbilityType;

import java.util.UUID;

public abstract class CardImpl extends HptcgObjectImpl implements Card {

    public CardImpl(UUID ownerId, CardSetInfo setInfo, CardType[] cardTypes, String costs) {
     this(ownerId, setInfo, cardTypes, costs, SpellAbilityType.BASE);
    }

    public CardImpl(UUID ownerId, CardSetInfo setInfo, CardType[] cardTypes, String costs, SpellAbilityType base) {

    }

    public Mana getManaContribution() {
        return new Mana();
    }

}
