package hptcg;

import hptcg.constants.CardType;

import java.io.Serializable;
import java.util.EnumSet;

public interface HptcgObject extends HptcgItem, Serializable {

    String getName();

    String getIdName();

    String getLogName();

    String getImageName();

    void setName(String name);

    EnumSet<CardType> getCardType();
}
