package hptcg;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GameService {

    private List<Game> gameList = new ArrayList<>();

    public Collection<GameDescription> getGameDescriptionList() {
        return getGameList().stream().map(Game::getGameDescription).collect(Collectors.toList());
    }

    private Collection<Game> getGameList() {
        if (gameList == null) {
            gameList = new ArrayList<>();
        }
        return gameList;
    }

    public void addGameDescription(GameDescription gameDescription) {
        gameList.add(new Game(gameDescription));
    }
}
