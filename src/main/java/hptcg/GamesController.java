package hptcg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;

import java.security.Principal;
import java.util.Collection;

@Controller
public class GamesController {

    private GameService service;
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    public void setMessagingTemplate(SimpMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @Autowired
    public void setService(GameService service) {
        this.service = service;
    }

    @SubscribeMapping
    public Collection<GameDescription> onSubscribe() {
        return service.getGameDescriptionList();
    }

    @SendTo("/games")
    @MessageMapping("/games/create")
    public Collection<GameDescription> createGame(final Principal principal, final GameDescription gameDescription) {
        service.addGameDescription(gameDescription);
        return service.getGameDescriptionList();
    }

}
